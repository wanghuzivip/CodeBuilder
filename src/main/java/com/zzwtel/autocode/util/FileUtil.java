package com.zzwtel.autocode.util;

import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 * 文件工具类
 * @author yangtonggan
 * @date 2016-3-9
 */
public class FileUtil {
	private static String encoding = "UTF-8";
	/**
	 * 创建目录
	 * @param dir,如果是目录命名规范则创建目录，
	 * 如果是文件名规范则创建其父目录
	 */
	public static void mkDir(String dir){
		try{
			File f = new File(dir);						
			if(dir.indexOf('.') > -1){				
				String parent = f.getParent();
				File p = new File(parent);
				if(!p.exists()){
					p.mkdirs();
				}
			}else if(!f.exists()){				
				f.mkdirs();
			}
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}
	
	/**
	 * 把字符串写入文件中，如果文件不存在则创建文件
	 */
	public static void write(File file ,String data){
		try{
			String path = file.getPath();
			//如果目录不存在则创建该目录
			mkDir(path);
			//String name = file.getName();
			if(!file.exists()){
				file.createNewFile();
			}
			FileUtils.write(file, data, encoding);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}
	
	/**
	 * 把字符串写入文件中，如果文件不存在则创建文件
	 */
	public static void write(String file ,String data){		
		System.out.println(file);
		write(new File(file),data);
	}
	
	/**
	 * 删除目录
	 * @param args
	 */
	public static void delDir(File directory){
		try{
			FileUtils.deleteDirectory(directory);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}
	
	/**
	 * 删除目录
	 * @param args
	 */
	public static void delDir(String dir){
		delDir(new File(dir));		
	}
	
	
	public static void main(String[] args){
		write("d:/abcx/t.txt","ddd");
		delDir("d:/abcx/");
	}

}
