package com.zzwtel.autocode.template.model;

import org.jsoup.nodes.Element;

/**
 * 菜单模型
 */
public abstract class MenuModel {
	//模型
	private String model;
	//当前菜单在父目录中的显示顺序
	private int index;
	//菜单级数
	private int level;
	//菜单名
	private String name;
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public abstract String toXml();
	
	
	
	public abstract MenuModel fromElement(Element ele,int index) throws RuntimeException;
	
}
