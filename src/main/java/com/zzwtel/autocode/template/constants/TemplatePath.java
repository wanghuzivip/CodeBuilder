package com.zzwtel.autocode.template.constants;

/**
 * 模板路径
 * @author yangtonggan
 * @date 2016-3-9
 */
public interface TemplatePath {
	//左菜单试图html页面模板路径
	public static String LEFT_MENU_HTML_TEMPLATE = "/left-menu/left_menu.html";
	//左菜单事件js页面模板路径
	public static String LEFT_MENU_JS_TEMPLATE = "/left-menu/left_menu_event.js";
	//添加表单html页面模板路径
	public static String ADD_FORM_HTML_TEMPLATE = "/html-layout1/@{entity_model}_add_form.html";
	//添加表单html页面模板路径-两列
	public static String ADD_FORM_2COLUMN_HTML_TEMPLATE = "/html-layout2/@{entity_model}_add_form.html";	
	//添加表单js模板路径
	public static String ADD_FORM_JS_TEMPLATE = "/js/@{entity_model}_add_form.js";
	//编辑表单html页面模板路径
	public static String EDIT_FORM_HTML_TEMPLATE = "/html-layout1/@{entity_model}_edit_form.html";
	//编辑表单html页面模板路径-两列
	public static String EDIT_FORM_2COLUMN_HTML_TEMPLATE = "/html-layout2/@{entity_model}_edit_form.html";
	//编辑表单js模板路径
	public static String EDIT_FORM_JS_TEMPLATE = "/js/@{entity_model}_edit_form.js";
	//java模板文件路径
	public static String JAVA_FILE_TEMPLATE = "/java/@{EntityModel}Controller.java";
	//列表html页面模板路径
	public static String LIST_HTML_TEMPLATE = "/html-list/@{entity_model}_list.html";
	//列表js模板生路径
	public static String LIST_JS_TEMPLATE = "/js/@{entity_model}_list.js";	
	//UrlConstantsTemplate模板路径
	public static String URL_CONSTANTS_TEMPLATE = "/java/UrlConstants.java";
	//UrlConstantsTemplate输出路径
	public static String URL_CONSTANTS_OUTPUT = "com/zzwtec/community/common/UrlConstants.java";
	//布局文件路径
	public static String LAYOUT_MODEL_FILE_PATH = "com/zzwtec/community/common/UrlConstants.java";
}
