package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.AddFormJsTemplate;
import com.zzwtel.autocode.template.model.UIModel;

public class AddFormJsBuilder {

	public void makeCode(UIModel vm,String modularDir) {
		AddFormJsTemplate template = new AddFormJsTemplate();
		template.generate(vm, modularDir);		
	}

}
