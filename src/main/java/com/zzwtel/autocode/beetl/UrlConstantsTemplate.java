package com.zzwtel.autocode.beetl;

import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;

import com.zzwtel.autocode.template.constants.TemplatePath;
import com.zzwtel.autocode.template.model.ControllerModel;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.template.model.UrlModel;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.HumpUtil;
import com.zzwtel.autocode.util.PathUtil;
import com.zzwtel.autocode.util.StrKit;

/**
 * Url常量模板类
 * @author yangtonggan
 * @date 2016-3-8
 */
public class UrlConstantsTemplate {
	/**
	 * 生成java代码
	 * @param m 数据模型
	 * @param template 模板路径
	 * 需要分清 首字母 大写 ，小写 ，全部大写 ，小写
	 */
	public void generate(ControllerModel m){
		try{			
			String root = PathUtil.getTemplateRoot();
			FileResourceLoader resourceLoader = new FileResourceLoader(root,"utf-8");
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			Template t = gt.getTemplate(TemplatePath.URL_CONSTANTS_TEMPLATE);		
			String model = m.getTable().getModel();			
			//首字母小写，驼峰命名
			t.binding("entityModel", model);
			//首字母大写，驼峰命名
			t.binding("EntityModel", StrKit.firstCharToUpperCase(model));
			//全部大写，下划线分割
			String _model = HumpUtil.underscoreName(model);
			t.binding("ENTITY_MODEL", _model);
			//全部小写，下划线分割			
			t.binding("entity_model", HumpUtil.toLowerCase(_model));			
			
			
			String data = t.render();			
			String out = PathUtil.getOutRoot();			
			String fileName = TemplatePath.URL_CONSTANTS_OUTPUT;		
			FileUtil.write(out+"java/"+fileName, data);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	

	public void generate(List<UrlModel> urls) {
		try{
			String root = PathUtil.getTemplateRoot();
			FileResourceLoader resourceLoader = new FileResourceLoader(root,"utf-8");
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			Template t = gt.getTemplate(TemplatePath.URL_CONSTANTS_TEMPLATE);	
			t.binding("urls", urls);			
			String data = t.render();			
			String out = PathUtil.getOutRoot();			
			String fileName = TemplatePath.URL_CONSTANTS_OUTPUT;		
			FileUtil.write(out+"java/"+fileName, data);			
		}catch(Exception e){
			
		}		
		
	}
	
	public static void main(String[] args){
		UrlConstantsTemplate jft = new UrlConstantsTemplate();
		ControllerModel m = new ControllerModel();
		m.setTable(new Table());
		m.getTable().setModel("area");
		jft.generate(m);
	}
}
