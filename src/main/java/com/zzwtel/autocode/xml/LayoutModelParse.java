package com.zzwtel.autocode.xml;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.zzwtel.autocode.util.PathUtil;

import us.codecraft.xsoup.Xsoup;

/**
 * 布局模型解析器
 * @author yangtonggan
 * @date 2016-3-10
 */
public class LayoutModelParse {
	private static Document document;
	/**
	 * 解析布局模型
	 */
	public static void parse(){
		try{
			if(document == null){
				String root = PathUtil.getModelRoot();
				File file = new File(root+"layout/layout-model.xml");
				document = Jsoup.parse(file, "UTF-8");
			}			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}
	
	public static Integer getColumns(String table){		
		try{
			parse();
			if(document != null){
				String result = Xsoup.select(document, "//layout[contains(@table,'"+table+"')]/form-layout/@column").get();
				if(result == null){
					return 1;
				}
				return Integer.parseInt(result);
			}
		}catch(Exception e){
			throw new RuntimeException("布局模型还没有生成，请先执行LayoutModelCreate");
		}	
		return -1;
	}
	
	public static void main(String[] args){		
		int x = getColumns("build");
		System.out.println(x);
	}
}
