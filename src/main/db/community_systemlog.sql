-- MySQL dump 10.13  Distrib 5.6.23, for Linux (x86_64)
--
-- Host: localhost    Database: community_systemlog
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_log`
--

drop database if exists `community_systemlog`;

create database `community_systemlog`;

use `community_systemlog`;


DROP TABLE IF EXISTS `account_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_log` (
  `id` varchar(32) NOT NULL,
  `account_id` varchar(32) NOT NULL,
  `deletec` bit(1) DEFAULT NULL,
  `opt` longtext NOT NULL,
  `opttime` bigint(20) NOT NULL,
  `opttype` smallint(6) NOT NULL,
  `remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_log`
--

LOCK TABLES `account_log` WRITE;
/*!40000 ALTER TABLE `account_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ad_admin_log`
--

DROP TABLE IF EXISTS `ad_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_admin_log` (
  `id` varchar(32) NOT NULL,
  `adadmin_id` varchar(32) NOT NULL,
  `advertiser_id` varchar(32) NOT NULL,
  `deletec` bit(1) DEFAULT NULL,
  `opt` longtext NOT NULL,
  `opttime` bigint(20) NOT NULL,
  `opttype` smallint(6) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_admin_log`
--

LOCK TABLES `ad_admin_log` WRITE;
/*!40000 ALTER TABLE `ad_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `ad_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_log`
--

DROP TABLE IF EXISTS `admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_log` (
  `id` varchar(32) NOT NULL,
  `admin_id` varchar(32) NOT NULL,
  `deletec` bit(1) DEFAULT NULL,
  `opt` longtext NOT NULL,
  `opttime` bigint(20) NOT NULL,
  `opttype` smallint(6) NOT NULL,
  `property_id` varchar(32) NOT NULL,
  `remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_log`
--

LOCK TABLES `admin_log` WRITE;
/*!40000 ALTER TABLE `admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `call_log`
--

DROP TABLE IF EXISTS `call_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_log` (
  `id` varchar(32) NOT NULL,
  `admdevice_id` varchar(32) DEFAULT NULL,
  `build_id` varchar(32) DEFAULT NULL,
  `calltime` bigint(20) NOT NULL,
  `cell_id` varchar(32) DEFAULT NULL,
  `community_id` varchar(32) DEFAULT NULL,
  `deletec` bit(1) DEFAULT NULL,
  `logmsg` longtext NOT NULL,
  `remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `call_log`
--

LOCK TABLES `call_log` WRITE;
/*!40000 ALTER TABLE `call_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `call_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comm_open_log`
--

DROP TABLE IF EXISTS `comm_open_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comm_open_log` (
  `id` varchar(32) NOT NULL,
  `admin_id` varchar(32) NOT NULL,
  `build_id` varchar(32) DEFAULT NULL,
  `build_name` varchar(255) DEFAULT NULL,
  `build_num` varchar(8) DEFAULT NULL,
  `community_id` varchar(32) DEFAULT NULL,
  `community_name` varchar(255) DEFAULT NULL,
  `deletec` bit(1) DEFAULT NULL,
  `indexs` smallint(6) DEFAULT NULL,
  `open_time` bigint(20) NOT NULL,
  `property_id` varchar(32) NOT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `worker_id` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comm_open_log`
--

LOCK TABLES `comm_open_log` WRITE;
/*!40000 ALTER TABLE `comm_open_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `comm_open_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-09 17:03:45
