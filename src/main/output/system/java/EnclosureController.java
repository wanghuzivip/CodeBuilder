package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Enclosure;
import com.zzwtec.community.model.EnclosureM;
import com.zzwtec.community.model.EnclosureMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.EnclosureServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Enclosure控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class EnclosureController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ENCLOSURE)
	public void list(){				
		//EnclosureServicePrx prx = IceServiceUtil.getService(EnclosureServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/enclosure_list.html");
	}
	
	/**
	 * 添加Enclosure
	 */
	@ActionKey(UrlConstants.URL_ENCLOSURE_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Enclosure成功");;
		try{
			//EnclosureServicePrx prx = IceServiceUtil.getService(EnclosureServicePrx.class);
			Enclosure model = getBean(Enclosure.class);		
			//prx.addEnclosure(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Enclosure失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Enclosure
	 */
	@ActionKey(UrlConstants.URL_ENCLOSURE_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Enclosure成功");
		try{
			String ids = getPara("ids");		
			//EnclosureServicePrx prx = IceServiceUtil.getService(EnclosureServicePrx.class);
			//prx.delEnclosureByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Enclosure失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ENCLOSURE_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Enclosure entity = new Enclosure();	
		setAttr("enclosure", entity);
		render("/system/view/enclosure_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ENCLOSURE_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//EnclosureServicePrx prx = IceServiceUtil.getService(EnclosureServicePrx.class);
			//EnclosureM modele = prx.inspectEnclosure(id);
			Enclosure entity = new Enclosure();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("enclosure", entity);			
			render("/system/view/enclosure_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Enclosure失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Enclosure
	 */
	@ActionKey(UrlConstants.URL_ENCLOSURE_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Enclosure成功");
		try{
			//EnclosureServicePrx prx = IceServiceUtil.getService(EnclosureServicePrx.class);
			Enclosure model = getBean(Enclosure.class);
			//prx.alterEnclosure(model);
		}catch(Exception e){
			repJson = new DataObject("更新Enclosure失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(EnclosureServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//EnclosureMPage viewModel = prx.getEnclosureList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Enclosure[] aray = new Enclosure[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Enclosure();
			aray[i].id = "id-"+i;			
		}	
		List<Enclosure> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
