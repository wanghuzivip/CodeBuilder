package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Msg;
import com.zzwtec.community.model.MsgM;
import com.zzwtec.community.model.MsgMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.MsgServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Msg控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class MsgController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_MSG)
	public void list(){				
		//MsgServicePrx prx = IceServiceUtil.getService(MsgServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/msg_list.html");
	}
	
	/**
	 * 添加Msg
	 */
	@ActionKey(UrlConstants.URL_MSG_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Msg成功");;
		try{
			//MsgServicePrx prx = IceServiceUtil.getService(MsgServicePrx.class);
			Msg model = getBean(Msg.class);		
			//prx.addMsg(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Msg失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Msg
	 */
	@ActionKey(UrlConstants.URL_MSG_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Msg成功");
		try{
			String ids = getPara("ids");		
			//MsgServicePrx prx = IceServiceUtil.getService(MsgServicePrx.class);
			//prx.delMsgByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Msg失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_MSG_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Msg entity = new Msg();	
		setAttr("msg", entity);
		render("/system/view/msg_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_MSG_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//MsgServicePrx prx = IceServiceUtil.getService(MsgServicePrx.class);
			//MsgM modele = prx.inspectMsg(id);
			Msg entity = new Msg();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("msg", entity);			
			render("/system/view/msg_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Msg失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Msg
	 */
	@ActionKey(UrlConstants.URL_MSG_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Msg成功");
		try{
			//MsgServicePrx prx = IceServiceUtil.getService(MsgServicePrx.class);
			Msg model = getBean(Msg.class);
			//prx.alterMsg(model);
		}catch(Exception e){
			repJson = new DataObject("更新Msg失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(MsgServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//MsgMPage viewModel = prx.getMsgList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Msg[] aray = new Msg[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Msg();
			aray[i].id = "id-"+i;			
		}	
		List<Msg> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
