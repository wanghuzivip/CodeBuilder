package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Role;
import com.zzwtec.community.model.RoleM;
import com.zzwtec.community.model.RoleMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.RoleServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Role控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class RoleController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ROLE)
	public void list(){				
		//RoleServicePrx prx = IceServiceUtil.getService(RoleServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/role_list.html");
	}
	
	/**
	 * 添加Role
	 */
	@ActionKey(UrlConstants.URL_ROLE_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Role成功");;
		try{
			//RoleServicePrx prx = IceServiceUtil.getService(RoleServicePrx.class);
			Role model = getBean(Role.class);		
			//prx.addRole(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Role失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Role
	 */
	@ActionKey(UrlConstants.URL_ROLE_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Role成功");
		try{
			String ids = getPara("ids");		
			//RoleServicePrx prx = IceServiceUtil.getService(RoleServicePrx.class);
			//prx.delRoleByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Role失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ROLE_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Role entity = new Role();	
		setAttr("role", entity);
		render("/system/view/role_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ROLE_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//RoleServicePrx prx = IceServiceUtil.getService(RoleServicePrx.class);
			//RoleM modele = prx.inspectRole(id);
			Role entity = new Role();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("role", entity);			
			render("/system/view/role_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Role失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Role
	 */
	@ActionKey(UrlConstants.URL_ROLE_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Role成功");
		try{
			//RoleServicePrx prx = IceServiceUtil.getService(RoleServicePrx.class);
			Role model = getBean(Role.class);
			//prx.alterRole(model);
		}catch(Exception e){
			repJson = new DataObject("更新Role失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(RoleServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//RoleMPage viewModel = prx.getRoleList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Role[] aray = new Role[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Role();
			aray[i].id = "id-"+i;			
		}	
		List<Role> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
