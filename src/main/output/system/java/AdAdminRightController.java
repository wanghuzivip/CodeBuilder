package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.AdAdminRight;
import com.zzwtec.community.model.AdAdminRightM;
import com.zzwtec.community.model.AdAdminRightMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdAdminRightServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * AdAdminRight控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdAdminRightController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_RIGHT)
	public void list(){				
		//AdAdminRightServicePrx prx = IceServiceUtil.getService(AdAdminRightServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/ad_admin_right_list.html");
	}
	
	/**
	 * 添加AdAdminRight
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_RIGHT_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加AdAdminRight成功");;
		try{
			//AdAdminRightServicePrx prx = IceServiceUtil.getService(AdAdminRightServicePrx.class);
			AdAdminRight model = getBean(AdAdminRight.class);		
			//prx.addAdAdminRight(model);				
		}catch(Exception e){
			repJson = new DataObject("添加AdAdminRight失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除AdAdminRight
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_RIGHT_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除AdAdminRight成功");
		try{
			String ids = getPara("ids");		
			//AdAdminRightServicePrx prx = IceServiceUtil.getService(AdAdminRightServicePrx.class);
			//prx.delAdAdminRightByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除AdAdminRight失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_RIGHT_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		AdAdminRight entity = new AdAdminRight();	
		setAttr("adAdminRight", entity);
		render("/system/view/ad_admin_right_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_RIGHT_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdAdminRightServicePrx prx = IceServiceUtil.getService(AdAdminRightServicePrx.class);
			//AdAdminRightM modele = prx.inspectAdAdminRight(id);
			AdAdminRight entity = new AdAdminRight();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("adAdminRight", entity);			
			render("/system/view/ad_admin_right_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取AdAdminRight失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改AdAdminRight
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_RIGHT_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新AdAdminRight成功");
		try{
			//AdAdminRightServicePrx prx = IceServiceUtil.getService(AdAdminRightServicePrx.class);
			AdAdminRight model = getBean(AdAdminRight.class);
			//prx.alterAdAdminRight(model);
		}catch(Exception e){
			repJson = new DataObject("更新AdAdminRight失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdAdminRightServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdAdminRightMPage viewModel = prx.getAdAdminRightList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		AdAdminRight[] aray = new AdAdminRight[12];		
		for(int i=0;i<12;i++){
			aray[i] = new AdAdminRight();
			aray[i].id = "id-"+i;			
		}	
		List<AdAdminRight> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
