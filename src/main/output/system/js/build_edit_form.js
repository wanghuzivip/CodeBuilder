$(function(){
	/**设置模态框底部按钮颜色*/
	$('#btn_ok').mouseover(function(){
		$('#btn_cancel').css({
			'background-color':'#fff',
			'color':'#90b0e4'
		});		
		$(this).css({
			'background-color':'#90b0e4',
			'color':'#fff'
		});		
	}); 
	/**设置模态框底部按钮颜色*/
	$('#btn_cancel').mouseover(function(){		
		$('#btn_ok').css({
			'background-color':'#fff',
			'color':'#90b0e4'
		});		
		$(this).css({
			'background-color':'#90b0e4',
			'color':'#fff'
		});
	});
	
	/**本段代码供参考，不是有效代码，小区下拉框选中事件*/
	$('form[name=cell_edit_from] select[name$=communityId]').on('change',		
		function(){
		var data = {};				
		data['communityId'] = $(this).val();
		data['buildId'] = $(':input[name=buildId][type=hidden]').val();		
		//alert(JSON.stringify(data));	
		$.ajax({
			type:"POST",
			url:ctx+"/cell/build",
			data:data,
			success:function(repJson){				
				//替换楼栋下拉框	 alert(JSON.stringify(repJson));			
				var select = $(':input[name$=buildId]');				
				//select.children('option').remove();				
				select.append(repJson.data);
			}
		});
		
		
	});	
	
	/**本段代码供参考，不是有效代码，楼栋下拉框选中事件*/
	$('form[name=cell_edit_from] select[name$=buildId]').on('change',
		function(){
		//alert('你选中了楼栋下拉框');
	});
	
	/**
	 * 点击关闭按钮，关闭当前弹出窗口
	 */
	$('#btn_cancel').click(function(){		
		if($("div.popup_window div#btn_cancel").length >= 0){
			$("div.popup_window").remove();
		} 		
	});
	
	/**
	 * 点击表单ok按钮
	 */
	$('#btn_ok').click(function(){		
		var inputs = $("form[name=build_edit_from] :input");	
		var data = {};		
		$(inputs).each(function(){
			var input = $(this);			
			data[input.attr('name')]=input.val();			
		});			
		$.ajax({
			type:"POST",
			url:ctx+"/build/upd",
			data:data,
			success:function(repJson){			
			  openTipLayer(repJson.msg);
			}
		});
		
	});
	
});