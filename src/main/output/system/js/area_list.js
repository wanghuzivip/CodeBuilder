$(function(){
	/**
	 * 点击搜索按钮
	 */
	$('div span.btn_search').click(function(){
		alert('你点击了搜索按钮');
	});	
	/**
	 * 点击表格上添加按钮
	 */
	$('#add_btn').click(function(){	 
		var data = {};		
		$.ajax({
			type:"POST",
			url:ctx+"/area/add/form_ui",
			data:data,
			success:function(htmlText){				
				$("div.popup_window").remove();
				addForm(htmlText);				
			    openWin('area_add_div');
			}
		});	

	});
	
	/**
	 * 点击表格上删除按钮
	 */
	$('#remove_btn').click(function(){				
		layer.confirm('确定要删除？', {
			icon: 3, 
			title:'提示' ,
			btn: ['确定','取消'] 
		}, function(index){		   
			remove(ctx+"/area/del");
		    layer.close(index);
		});


	});
	
	/**
	 * 双击表格弹出修改窗口
	 */
	$('table > tbody > tr').dblclick(function(){		
		var id = $(this).find(':checkbox:eq(0)').val();		
		var data = {};
		data['id'] = id;		
		$.ajax({
			type:"POST",
			url:ctx+"/area/edit/form_ui",
			data:data,
			success:function(htmlText){				
				$("div.popup_window").remove();
				addForm(htmlText);			
				openWin('area_edit_div');
			}
		});
	});
	
	/**
	 * 实现分页条件查询
	 */
	$('ul.pagination > li > a').click(function(){
		var data = {};		
		data.name = $(':input[name=search]').val();
		var href = $(this).attr('href');		
		var str = href.substring(1,href.Length);		
		data.page=parseInt(str);		
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/area/list",
			data:data,
			success:function(msg){
				addPage(msg);
			}
		});
		
	});
	
});	